#include <QGuiApplication>
#include <QQmlApplicationEngine>

// GStreamer
#include <gst/gst.h>

int main(int argc, char *argv[])
{

    /* setup env variables */
    putenv("GST_PLUGIN_PATH=C:\\gstreamer\\1.0\\x86_64\\lib\\gstreamer-1.0") ;
    std::string envPath = "Path=" + std::string(getenv("Path")) + "C:\\gstreamer\\1.0\\x86_64\\bin;C:\\gstreamer;";
    putenv(envPath.c_str());

    /* Initializes the GStreamer library */
    GError *err;
    if (!gst_init_check(NULL, NULL, &err)){
        g_printerr ("gst_init_check returned FALSE\n");
        if(err) {
            fprintf (stderr, "err->message: %s\n", err->message);
        }
    }

    /* Create gstreamer elements */
    GstElement *pipeline, *source, *demuxer, *decoder, *conv, *sink;
    pipeline = gst_pipeline_new ("audio-player");
    source   = gst_element_factory_make ("filesrc",       "file-source");
    demuxer  = gst_element_factory_make ("oggdemux",      "ogg-demuxer");
    decoder  = gst_element_factory_make ("vorbisdec",     "vorbis-decoder");
    conv     = gst_element_factory_make ("audioconvert",  "converter");
    sink     = gst_element_factory_make ("autoaudiosink", "audio-output");

    if (!pipeline) {
        g_printerr ("pipeline == nullptr\n");
    }

    if (!source) {
        g_printerr ("source == nullptr\n");
    }

    if (!demuxer) {
        g_printerr ("demuxer == nullptr\n");
    }

    if (!decoder) {
        g_printerr ("decoder == nullptr\n");
    }

    if (!conv) {
        g_printerr ("conv == nullptr\n");
    }

    if (!sink) {
        g_printerr ("sink == nullptr\n");
    }

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
